<header>
Hello World
</header>

This is the first post of many to come.

I started WordPress as a “user” and after about a year I started Freelancing my skills on Upwork. 

This exposed me to the various aspects of WordPress as against the usual “blogging software” I knew it to originally be.

And like the WordPress 5.0 which marked the foundation of the future, this post marks the beginning of a journey that deeply excites me. 

Over the last four years, I have experienced and learnt a lot but I still have a lot more to learn and explore. 

So moving forward you will find resources and tutorials that revolve around a lot WordPress. 

I am sincerely excited and I will see you in subsequent posts. 