### Introduction ###

Docker is an application that allows us to deploy programs that are run as containers. It was written in the popular Go programming language. This tutorial explains how to install Docker CE on Ubuntu 18.04 LTS (Bionic Beaver). This tutorial also works on Ubuntu 18.10 (Cosmic Cuttlefish).

### Step 1: Uninstall old versions ###

Older versions of Docker were called `docker`, `docker.io`, or `docker-engine`. If these are installed in your machine, you can uninstall them:

    sudo apt-get remove docker docker-engine docker.io containerd runc

It’s OK if `apt-get` reports that none of these packages are installed.

### Step 2: Updating all your software ###
Let’s make sure that we are using a clean system. Update the apt package index:

    sudo apt-get update

### Step 3: Set up the repository ###

Install packages to allow apt to use a repository over HTTPS:

    sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

Add Docker’s official GPG key:

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

**OUTPUT**

    OK

Verify that you now have the key with the fingerprint `9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88`, by searching for the last 8 characters of the fingerprint.

    sudo apt-key fingerprint 0EBFCD88

**OUTPUT**

    pub rsa4096 2017-02-22 [SCEA]
    9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88
    uid [ unknown] Docker Release (CE deb) <docker@docker.com>
    sub rsa4096 2017-02-22 [S]

Use the following command to set up the stable repository

    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

### Step 4: Install Docker CE ###

Update the apt package index.

    sudo apt-get update

Install the latest version of Docker CE and containerd.

    sudo apt-get install docker-ce docker-ce-cli containerd.io

### Step 5: Create a User ###

The docker group is created but no users are added to it. You need to use sudo to run Docker commands. 
It is not always recommended to run commands as root, in this case, you should create a non-root user which will be added to the docker group.

    adduser user

    usermod -aG docker user

Restart the Docker service.

    systemctl restart docker

### Step 6: Test Docker ###

Run the Docker hello-world container to test if the installation completed successfully.

    docker run hello-world

**OUTPUT**

    Hello from Docker!

    This message shows that your installation appears to be working correctly.

### Step 7: Configure Docker to start on boot ###

Lastly, enable Docker to run when your system boots.

    systemctl enable docker

Next steps
Congratulations! You have successfully installed Docker.

To further explore Docker, hop on the [official documentation](https://docs.docker.com/get-started/) to get started.

*Written by Promise Akpan*