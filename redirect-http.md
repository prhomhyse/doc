<header>
Redirect all HTTP requests to HTTPS
</header>

[HTTPS (Hypertext Transfer Protocol Secure)](https://support.google.com/webmasters/answer/6073543?hl=en) is an internet communication protocol that protects the integrity and confidentiality of data between the user’s computer and the site. Users expect a secure and private online experience when using a website. HTTPS is widely encouraged in order to protect your users’ connections to your website, regardless of the content on the site.

A service like Let’s Encrypt champions the movement to ensure all websites on the internet are served from HTTPS by default.
Let’s Encrypt is a free, automated, and open certificate authority (CA), run for the public’s benefit.

Before you proceed further, I assume you are already issued or purchased an SSL certificate and it is installed on your web server.

There are two most popular opensource web servers, Apache and Nginx.

### Apache ###

The vast majority of Apache HTTP Server instances run on a Linux distribution, but current versions also run on Windows and a wide variety of Unix-like systems. So if you are using a Shared Hosting option, you are most likely using Apache.

[.htaccess](http://www.htaccess-guide.com/) is a configuration file for use on web servers running the Apache. The .htaccess file alters the configuration of the Apache Web Server software to enable/disable additional functionality and features that the Apache Web Server software has to offer. 

You redirect all of your HTTP traffic to HTTPS by adding the following code to your .htaccess file.

    RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]


### Nginx ###

Nginx is a free, open-source, high-performance HTTP server and reverse proxy, as well as an IMAP/POP3 proxy server. … Unlike traditional servers, NGINX doesn’t rely on threads to handle requests. Instead, it uses a much more scalable event-driven (asynchronous) architecture. A thing worthy of note is Nginx does not use .htaccess files.

Unfortunately, most web hosts do not offer Nginx by default. In most cases, you have to build and set it up yourself.

You can easily redirect all of your HTTP traffic to HTTPS by adding the following code to the top of your Nginx config file.


    server {

    listen 80;

    server_name domain.com www.domain.com;

    return 301 https://domain.com$request_uri;

    }
    
We now have our HTTP traffic redirected by default to HTTPS. You may see an “insecure” on your browser. This is due to files being served as HTTP instead of HTTPS.